#include <asiosys.h>
#include <asio.h>
#include <asiodrivers.h>
#include <cstdlib>
#include <cstdio>
#include <Windows.h>

#define NUM_CHANNELS (2)

ASIOBufferInfo asioBufferInfo[NUM_CHANNELS * 2];
long blockSize;
size_t sampleSize;
int numCallbacks;
DWORD firstCallbackTicks;
DWORD lastCallbackTicks;

size_t GetSampleSize(ASIOSampleType asioSampleType)
{
	switch(asioSampleType)
	{
	case ASIOSTInt16MSB:
	case ASIOSTInt16LSB:
		return 2;
	case ASIOSTInt24MSB:
	case ASIOSTInt24LSB:
		return 3;
	case ASIOSTInt32MSB:
	case ASIOSTInt32LSB:
	case ASIOSTFloat32MSB:
	case ASIOSTFloat32LSB:
	case ASIOSTInt32MSB16:
	case ASIOSTInt32LSB16:
	case ASIOSTInt32MSB18:
	case ASIOSTInt32LSB18:
	case ASIOSTInt32MSB20:
	case ASIOSTInt32LSB20:
	case ASIOSTInt32MSB24:
	case ASIOSTInt32LSB24:
		return 4;
	case ASIOSTFloat64MSB:
	case ASIOSTFloat64LSB:
		return 8;
	case ASIOSTDSDInt8LSB1:
	case ASIOSTDSDInt8MSB1:
	case ASIOSTDSDInt8NER8:
		return 1;
	}

	return 0;
}

ASIOTime* bufferSwitchTimeInfo(ASIOTime* params, long doubleBufferIndex, ASIOBool directProcess)
{
	DWORD ticks = GetTickCount();
	if(firstCallbackTicks == 0)
		firstCallbackTicks = ticks;
	lastCallbackTicks = ticks;
	numCallbacks++;

	for(int i = 0; i < NUM_CHANNELS; i++)
	{
		memcpy(asioBufferInfo[NUM_CHANNELS + i].buffers[doubleBufferIndex], asioBufferInfo[i].buffers[doubleBufferIndex], blockSize * sampleSize);
	}

	return 0;
}

void bufferSwitch(long doubleBufferIndex, ASIOBool directProcess)
{
	bufferSwitchTimeInfo(NULL, doubleBufferIndex, directProcess);
}

void sampleRateDidChange(ASIOSampleRate sRate)
{
}

long asioMessage(long selector, long value, void* message, double* opt)
{
	long ret = 0;
	switch(selector)
	{
		case kAsioSelectorSupported:
			if(value == kAsioResetRequest
			|| value == kAsioEngineVersion
			|| value == kAsioResyncRequest
			|| value == kAsioLatenciesChanged
			|| value == kAsioSupportsTimeInfo
			|| value == kAsioSupportsTimeCode
			|| value == kAsioSupportsInputMonitor)
				ret = 1L;
			break;
		case kAsioResetRequest:
			ret = 1L;
			break;
		case kAsioEngineVersion:
			ret = 2L;
			break;
		case kAsioSupportsTimeInfo:
			ret = 1;
			break;
	}
	return ret;
}

extern AsioDrivers* asioDrivers;
bool loadAsioDriver(char *name);

bool RunTest(char *deviceName, ASIOSampleRate sampleRate, int testLength)
{
	ASIOError error;
	numCallbacks = 0;
	firstCallbackTicks = 0;
	lastCallbackTicks = 0;

	if (!loadAsioDriver(deviceName))
	{
		printf("Unable to load driver\n");
		return false;
	}

	ASIODriverInfo asioDriverInfo = { 0 };
	asioDriverInfo.asioVersion = 2;
	if((error = ASIOInit(&asioDriverInfo)) != ASE_OK)
	{
		printf("ASIOInit error %d\n", error);
		return false;
	}

	if(sampleRate > 0)
		if((error = ASIOSetSampleRate(sampleRate)) != ASE_OK)
		{
			printf("ASIOSetSampleRate erorr %d\n", error);
			return false;
		}

	ASIOCallbacks asioCallbacks = { bufferSwitch, sampleRateDidChange, asioMessage, bufferSwitchTimeInfo };

	for(int i = 0; i < NUM_CHANNELS; i++)
	{
		asioBufferInfo[i].isInput = ASIOTrue;
		asioBufferInfo[NUM_CHANNELS + i].isInput = ASIOFalse;

		asioBufferInfo[i].channelNum = i;
		asioBufferInfo[NUM_CHANNELS + i].channelNum = i;

		asioBufferInfo[i].buffers[0] = asioBufferInfo[i].buffers[1] = NULL;
		asioBufferInfo[NUM_CHANNELS + i].buffers[0] = asioBufferInfo[NUM_CHANNELS + i].buffers[1] = NULL;
	}

	if((error = ASIOCreateBuffers(asioBufferInfo, NUM_CHANNELS * 2, blockSize, &asioCallbacks)) != ASE_OK)
	{
		printf("ASIOCreateBuffers error %d\n", error);
		return false;
	}

	ASIOChannelInfo asioChannelInfo = { 0 };
	asioChannelInfo.channel = 0;
	asioChannelInfo.isInput = ASIOTrue;
	if((error = ASIOGetChannelInfo(&asioChannelInfo)) != ASE_OK)
	{
		printf("ASIOGetChannelInfo error %d\n", error);
		return false;
	}

	sampleSize = GetSampleSize(asioChannelInfo.type);
	if(sampleSize == 0)
	{
		printf("Unknown sample type %d\n", asioChannelInfo.type);
		return false;
	}

	if((error = ASIOStart()) != ASE_OK)
	{
		printf("ASIOStart error %d\n", error);
		return false;
	}

	Sleep(testLength * 1000);

	if((error = ASIOStop()) != ASE_OK)
		printf("ASIOStop error %d\n", error);

	if((error = ASIODisposeBuffers()) != ASE_OK)
		printf("ASIODisposeBuffers error %d\n", error);

	if((error = ASIOExit()) != ASE_OK)
		printf("ASIOExit error %d\n", error);

	asioDrivers->removeCurrentDriver();

	if(testLength > 0 && numCallbacks > 1)
		printf("Average callback period: %f ms (expected %f ms)\n", (double)(lastCallbackTicks - firstCallbackTicks) / (numCallbacks - 1), (1000. * blockSize) / sampleRate);

	return true;
}

int main(int argc, char *argv[])
{
	if(argc <= 2)
	{
		printf("Usage: FocusriteASIOTest.exe ASIODevice BufferSize [TestLength [SampleRate1 [SampleRate2]]]\n");
		return -1;
	}

	blockSize = atoi(argv[2]);

	int testLength = 10;
	
	if(argc > 3)
		testLength = atoi(argv[3]);
	
	ASIOSampleRate sampleRate = 0;
	ASIOSampleRate sampleRate2 = 0;

	if(argc > 4)
		sampleRate = atof(argv[4]);

	if(argc > 5)
		sampleRate2 = atof(argv[5]);

	if(!RunTest(argv[1], sampleRate, sampleRate2 > 0 ? 0 : testLength))  //If a second sample rate has been specified, just start and immediately stop with the first...
		return -1;

	if(sampleRate2 > 0)  //...then run the actual test with the second
		if(!RunTest(argv[1], sampleRate2, testLength))
			return -1;

	return 0;
}